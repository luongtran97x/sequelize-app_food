/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `food` (
  `food_id` int NOT NULL AUTO_INCREMENT,
  `food_name` varchar(255) NOT NULL,
  `image` varchar(150) NOT NULL,
  `price` float NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `type_id` int DEFAULT NULL,
  PRIMARY KEY (`food_id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `food_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `food_type` (`type_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `food_type` (
  `type_id` int NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `like_res` (
  `user_id` int DEFAULT NULL,
  `res_id` int DEFAULT NULL,
  `date_like` datetime DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  `like_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `res_id` (`res_id`),
  CONSTRAINT `like_res_ibfk_23` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE,
  CONSTRAINT `like_res_ibfk_24` FOREIGN KEY (`res_id`) REFERENCES `restaurant` (`res_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orders` (
  `user_id` int DEFAULT NULL,
  `food_id` int DEFAULT NULL,
  `amount` int NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `arr_sub_id` varchar(100) DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `food_id` (`food_id`),
  CONSTRAINT `orders_ibfk_23` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE,
  CONSTRAINT `orders_ibfk_24` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rate_res` (
  `user_id` int DEFAULT NULL,
  `res_id` int DEFAULT NULL,
  `amount` int NOT NULL,
  `date_rate` datetime NOT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `res_id` (`res_id`),
  CONSTRAINT `rate_res_ibfk_23` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `rate_res_ibfk_24` FOREIGN KEY (`res_id`) REFERENCES `restaurant` (`res_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `restaurant` (
  `res_id` int NOT NULL AUTO_INCREMENT,
  `res_name` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`res_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sub_food` (
  `sub_id` int NOT NULL AUTO_INCREMENT,
  `sub_name` varchar(100) NOT NULL,
  `sub_price` float NOT NULL,
  `food_id` int DEFAULT NULL,
  PRIMARY KEY (`sub_id`),
  KEY `food_id` (`food_id`),
  CONSTRAINT `sub_food_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `food` (`food_id`, `food_name`, `image`, `price`, `description`, `type_id`) VALUES
(1, 'Hột vịt lộn xào me', 'img/picture1', 100, 'ngon nhức nách', 1);
INSERT INTO `food` (`food_id`, `food_name`, `image`, `price`, `description`, `type_id`) VALUES
(2, 'Cá lóc nướng', 'img/picture2', 200, 'ngon nhức nách', 2);
INSERT INTO `food` (`food_id`, `food_name`, `image`, `price`, `description`, `type_id`) VALUES
(3, 'Gà hấp bia', 'img/picture3', 300, 'ngon nhức nách', 3);
INSERT INTO `food` (`food_id`, `food_name`, `image`, `price`, `description`, `type_id`) VALUES
(4, 'Tôm chiên bột', 'img/picture2', 250, 'ngon nhức nách', 4),
(5, 'Vịt quay', 'img/picture4', 400, 'ngon nhức nách', 5),
(6, 'Hào nướng phô mai', 'img/picture6', 150, 'ngon nhức nách', 2),
(7, 'Ếch núp lùm', 'img/picture2', 120, 'ngon nhức nách', 1),
(8, 'Cá lóc nướng', 'img/picture2', 200, 'ngon nhức nách', 2),
(9, 'Hột vịt lộn xào me', 'img/picture1', 100, 'ngon nhức nách', 1),
(10, 'Cá lóc nướng', 'img/picture2', 200, 'ngon nhức nách', 2),
(11, 'Gà hấp bia', 'img/picture3', 300, 'ngon nhức nách', 3),
(12, 'Tôm chiên bột', 'img/picture2', 250, 'ngon nhức nách', 4),
(13, 'Vịt quay', 'img/picture4', 400, 'ngon nhức nách', 5),
(14, 'Hào nướng phô mai', 'img/picture6', 150, 'ngon nhức nách', 2),
(15, 'Ếch núp lùm', 'img/picture2', 120, 'ngon nhức nách', 1),
(16, 'Cá lóc nướng', 'img/picture2', 200, 'ngon nhức nách', 2);

INSERT INTO `food_type` (`type_id`, `type_name`) VALUES
(1, 'xào');
INSERT INTO `food_type` (`type_id`, `type_name`) VALUES
(2, 'nướng');
INSERT INTO `food_type` (`type_id`, `type_name`) VALUES
(3, 'hấp');
INSERT INTO `food_type` (`type_id`, `type_name`) VALUES
(4, 'chiên'),
(5, 'quay'),
(6, 'xào'),
(7, 'nướng'),
(8, 'hấp'),
(9, 'chiên'),
(10, 'quay');

INSERT INTO `like_res` (`user_id`, `res_id`, `date_like`, `id`, `like_status`) VALUES
(1, 2, '2023-10-09 08:02:02', 38, 1);


INSERT INTO `orders` (`user_id`, `food_id`, `amount`, `code`, `arr_sub_id`, `id`) VALUES
(1, 1, 10, '01', 'abcd', 1);
INSERT INTO `orders` (`user_id`, `food_id`, `amount`, `code`, `arr_sub_id`, `id`) VALUES
(1, 2, 5, '01', 'abcd', 2);
INSERT INTO `orders` (`user_id`, `food_id`, `amount`, `code`, `arr_sub_id`, `id`) VALUES
(1, 3, 2, '01', 'abcd', 3);
INSERT INTO `orders` (`user_id`, `food_id`, `amount`, `code`, `arr_sub_id`, `id`) VALUES
(1, 4, 10, '01', 'abcd', 4),
(1, 5, 2, '01', 'abcd', 5),
(2, 1, 1, '01', 'abcd', 6),
(2, 2, 2, '01', 'abcd', 7),
(2, 3, 3, '01', 'abcd', 8),
(2, 4, 4, '01', 'abcd', 9),
(2, 5, 5, '01', 'abcd', 10),
(3, 6, 6, '02', 'abcd', 11),
(3, 7, 7, '02', 'abcd', 12),
(3, 8, 8, '02', 'abcd', 13),
(3, 1, 2, '02', 'abcd', 14),
(3, 3, 2, '02', 'abcd', 15),
(4, 6, 6, '02', 'abcd', 16),
(4, 5, 9, '02', 'abcd', 17),
(4, 1, 2, '02', 'abcd', 18),
(4, 8, 3, '02', 'abcd', 19),
(4, 7, 1, '02', 'abcd', 20),
(5, 8, 5, '02', 'abcd', 21),
(5, 1, 4, '02', 'abcd', 22),
(5, 2, 3, '02', 'abcd', 23),
(5, 6, 7, '02', 'abcd', 24),
(5, 7, 8, '02', 'abcd', 25),
(6, 6, 6, '02', 'abcd', 26),
(6, 2, 3, '02', 'abcd', 27),
(6, 4, 1, '02', 'abcd', 28),
(6, 3, 3, '02', 'abcd', 29),
(6, 7, 9, '02', 'abcd', 30),
(6, 5, 6, '02', 'abcd', 31),
(7, 6, 6, '02', 'abcd', 32),
(7, 7, 7, '02', 'abcd', 33),
(7, 8, 7, '02', 'abcd', 34),
(7, 1, 7, '02', 'abcd', 35),
(7, 2, 7, '02', 'abcd', 36),
(1, 1, 10, '01', 'abcd', 37),
(1, 2, 5, '01', 'abcd', 38),
(1, 3, 2, '01', 'abcd', 39),
(1, 4, 10, '01', 'abcd', 40),
(1, 5, 2, '01', 'abcd', 41),
(2, 1, 1, '01', 'abcd', 42),
(2, 2, 2, '01', 'abcd', 43),
(2, 3, 3, '01', 'abcd', 44),
(2, 4, 4, '01', 'abcd', 45),
(2, 5, 5, '01', 'abcd', 46),
(3, 6, 6, '02', 'abcd', 47),
(3, 7, 7, '02', 'abcd', 48),
(3, 8, 8, '02', 'abcd', 49),
(3, 1, 2, '02', 'abcd', 50),
(3, 3, 2, '02', 'abcd', 51),
(4, 6, 6, '02', 'abcd', 52),
(4, 5, 9, '02', 'abcd', 53),
(4, 1, 2, '02', 'abcd', 54),
(4, 8, 3, '02', 'abcd', 55),
(4, 7, 1, '02', 'abcd', 56),
(5, 8, 5, '02', 'abcd', 57),
(5, 1, 4, '02', 'abcd', 58),
(5, 2, 3, '02', 'abcd', 59),
(5, 6, 7, '02', 'abcd', 60),
(5, 7, 8, '02', 'abcd', 61),
(6, 6, 6, '02', 'abcd', 62),
(6, 2, 3, '02', 'abcd', 63),
(6, 4, 1, '02', 'abcd', 64),
(6, 3, 3, '02', 'abcd', 65),
(6, 7, 9, '02', 'abcd', 66),
(6, 5, 6, '02', 'abcd', 67),
(7, 6, 6, '02', 'abcd', 68),
(7, 7, 7, '02', 'abcd', 69),
(7, 8, 7, '02', 'abcd', 70),
(7, 1, 7, '02', 'abcd', 71),
(7, 2, 7, '02', 'abcd', 72),
(7, 1, 2, NULL, NULL, 73),
(7, 1, 2, NULL, NULL, 74),
(7, 1, 2, NULL, NULL, 75),
(1, 2, 3, NULL, NULL, 76),
(1, 2, 3, NULL, NULL, 77),
(1, 2, 3, NULL, NULL, 78);

INSERT INTO `rate_res` (`user_id`, `res_id`, `amount`, `date_rate`, `id`) VALUES
(1, 1, 5, '2021-05-06 12:02:02', 1);
INSERT INTO `rate_res` (`user_id`, `res_id`, `amount`, `date_rate`, `id`) VALUES
(2, 2, 4, '2021-05-06 12:02:02', 2);
INSERT INTO `rate_res` (`user_id`, `res_id`, `amount`, `date_rate`, `id`) VALUES
(3, 1, 2, '2021-05-06 12:02:02', 3);
INSERT INTO `rate_res` (`user_id`, `res_id`, `amount`, `date_rate`, `id`) VALUES
(4, 3, 5, '2021-05-06 12:02:02', 4),
(5, 2, 1, '2021-05-06 12:02:02', 5),
(6, 3, 5, '2021-05-06 12:02:02', 6),
(1, 1, 2, '2021-05-06 05:02:02', 7),
(1, 1, 2, '2023-10-07 17:00:00', 8);

INSERT INTO `restaurant` (`res_id`, `res_name`, `image`, `description`) VALUES
(1, 'SanFuLou', 'img/picture1', 'nhà hàng người hoa');
INSERT INTO `restaurant` (`res_id`, `res_name`, `image`, `description`) VALUES
(2, 'GogiHouse', 'img/picture2', 'nướng hàn quốc');
INSERT INTO `restaurant` (`res_id`, `res_name`, `image`, `description`) VALUES
(3, 'ThaiExpress', 'img/picture3', 'nhà hàng kiểu Thái');
INSERT INTO `restaurant` (`res_id`, `res_name`, `image`, `description`) VALUES
(4, 'SanFuLou', 'img/picture1', 'nhà hàng người hoa'),
(5, 'GogiHouse', 'img/picture2', 'nướng hàn quốc'),
(6, 'ThaiExpress', 'img/picture3', 'nhà hàng kiểu Thái');



INSERT INTO `user` (`user_id`, `full_name`, `email`, `password`) VALUES
(1, 'Trịnh Đình G', 'trinhdinhg@gmail.com', 'trinhdinhg');
INSERT INTO `user` (`user_id`, `full_name`, `email`, `password`) VALUES
(2, 'Ngô Quang H', 'ngoquangh@gmail.com', 'ngoquangh');
INSERT INTO `user` (`user_id`, `full_name`, `email`, `password`) VALUES
(3, 'Nguyễn Văn A', 'nguyenvanA@gmail.com', 'nguyenvana');
INSERT INTO `user` (`user_id`, `full_name`, `email`, `password`) VALUES
(4, 'Trần Văn B', 'tranvanb@gmail.com', 'tranvanb'),
(5, 'Phạm Thị C', 'phamthic@gmail.com', 'phamthic'),
(6, 'Truơng Văn D', 'truongvand@gmail.com', 'truongvand'),
(7, 'Đỗ Văn E', 'dovane@gmail.com', 'dovane'),
(8, 'Hà Văn F', 'havanf@gmail.com', 'havanf'),
(9, 'Lê Văn G', 'levang@gmail.com', 'levang'),
(10, 'Trịnh Đình G', 'trinhdinhg@gmail.com', 'trinhdinhg'),
(11, 'Ngô Quang H', 'ngoquangh@gmail.com', 'ngoquangh'),
(12, 'Nguyễn Văn A', 'nguyenvanA@gmail.com', 'nguyenvana'),
(13, 'Trần Văn B', 'tranvanb@gmail.com', 'tranvanb'),
(14, 'Phạm Thị C', 'phamthic@gmail.com', 'phamthic'),
(15, 'Truơng Văn D', 'truongvand@gmail.com', 'truongvand'),
(16, 'Đỗ Văn E', 'dovane@gmail.com', 'dovane'),
(17, 'Hà Văn F', 'havanf@gmail.com', 'havanf'),
(18, 'Lê Văn G', 'levang@gmail.com', 'levang');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;