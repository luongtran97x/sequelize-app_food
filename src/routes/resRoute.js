import express from "express";
import {  getLikeRes, getLikeUser, getRateRes, getRateUser, handelOrder, likeRes, rateRes,  } from "../controllers/resController.js";

const resRoute = express.Router();

// api danh sách like theo id nhà hàng
resRoute.get("/get-res-like/:id",getLikeRes)

// api danh sách like theo id user 
resRoute.get("/get-user-like/:id",getLikeUser)

// api chức năng like nhà hàng 
resRoute.post("/like-res",likeRes)


/*------------------------------------------*/
// api order
resRoute.post("/order",handelOrder)

/* ----------------------------------------*/

// api danh sách rate theo id nhà hàng
resRoute.get("/get-res-rate/:id",getRateRes);

// api danh sách rate theo id  user
resRoute.get("/get-user-rate/:id",getRateUser);

// api thêm đánh giá nhà hàng
resRoute.post("/rate-res",rateRes)


export default resRoute;
